<?php
/*
 * Template name: TPL Homepage
 */

$post = new TimberPost();
$context = Timber::get_context();
$detect = new Mobile_Detect();
$context['post'] = $post;
$context['template_path'] = get_template_directory_uri();
$context['slider'] = (have_rows('slider')) ? get_field('slider') : array();
$context['url_plan'] = site_url( '/list-lots/' );
$context['url_contact'] = site_url( '/contact/' );
$context['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false;
$context['navigation_home'] = wp_nav_menu(array('menu' => 'homepage_nav', 'echo' => false));

/*echo ($context['mobile_device']) ? 'IS MOBILE' : 'NOT MOBILE';exit();*/

Timber::render( array( 'page-' . $post->post_name . '.twig', 'homepage.twig' ), $context );