<?php

/*
 * 
 */
class MeoCrmCoreHelper
{
    /*
     *  Generate a flash message for the administration
     * 
     *  @param $message(string), $type(integer) => 0 = erreur & 1 = valide
     *  @return $flash_message(string) => html of flash message 
     */
    public function getFlashMessageAdmin($message,$type)
    {
        if($type)
        {
            $class = 'updated notice is-dismissible';
        }else{
            $class = 'error';
        }
        $flash_message   = '';
        $flash_message  .= '<div class="'.$class.'" style="margin-left: 0px !important">';
        $flash_message  .=      '<p>';
        $flash_message  .=          $message;
        $flash_message  .=      '</p>';
        $flash_message  .= '</div>';
        
        return $flash_message;
    }
    
    /*
     *  Convert a date to data base date
     * 
     *  @param $message(string), $type(integer) => 0 = erreur & 1 = valide
     *  @return $flash_message(string) => html of flash message 
     */
    public static function convertDateFormatToDbFormat($date,$format,$withHours=false,$hours='00:00:00')
    {
        switch($format)
        {
            case 'd/m/y':
                list($day,$month,$year) = explode('/',$date);
                break;
            
            case 'm/d/y':
                list($month,$day,$year) = explode('/',$date);
                break;
            
            case 'd.m.y':
                list($day,$month,$year) = explode('.',$date);
                break;
            
            case 'm.d.y':
                list($month,$day,$year) = explode('.',$date);
                break;
            
            case 'd-m-y':
                list($day,$month,$year) = explode('-',$date);
                break;
            
            case 'm-d-y':
                list($month,$day,$year) = explode('-',$date);
                break;
        }
        
        if($withHours)
        {
            $dateConvert = $year.'-'.$month.'-'.$day.' '.$hours;
        }else{
            $dateConvert = $year.'-'.$month.'-'.$day.'';
        }
                
        return $dateConvert;
    }

    public static function getExternalDbConnection($db_login,$db_password,$db_name,$db_host)
    {
        // Creation acces DB externe
        if(!empty($db_login) && !empty($db_password) && !empty($db_name) && !empty($db_host))
        {
            $external_wpdb = new wpdb($db_login,$db_password,$db_name,$db_host);
            return $external_wpdb;
        }
    }

    public static function getExternalFtpConnection($ftp_login,$ftp_password,$ftp_host)
    {
        // Creation acces FTP externe
        if(!empty($ftp_host) && !empty($ftp_login) && !empty($ftp_password))
        {
            $ftp = ftp_connect($ftp_host, 21);
            ftp_login($ftp, $ftp_login, $ftp_password);

            return $ftp;
        }
    }
    public static function hex2rgb( $color )
    {
        if ( $color[0] == '#' ) {
            $color = substr( $color, 1 );
        }

        if ( strlen( $color ) == 6 ) {
            list( $r, $g, $b ) = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
            list( $r, $g, $b ) = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
            return false;
        }

        $r = hexdec( $r );
        $g = hexdec( $g );
        $b = hexdec( $b );
        return array( 'red' => $r, 'green' => $g, 'blue' => $b );
    }
}


